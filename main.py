import requests

def main():
    response = requests.get("http://some_host:80")
    response.raise_for_status()
    return response.text

if __name__ == "__main__":
    result = main()
    print(result)
